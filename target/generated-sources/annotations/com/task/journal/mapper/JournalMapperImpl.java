package com.task.journal.mapper;

import com.task.journal.dto.GroupDto;
import com.task.journal.dto.MarkDto;
import com.task.journal.dto.StudentDto;
import com.task.journal.dto.SubjectDto;
import com.task.journal.entity.Group;
import com.task.journal.entity.MarkEntity;
import com.task.journal.entity.StudentEntity;
import com.task.journal.entity.SubjectEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-12T20:05:12+0600",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class JournalMapperImpl implements JournalMapper {

    @Override
    public StudentEntity studentDtoToStudentEntity(StudentDto studentDto) {
        if ( studentDto == null ) {
            return null;
        }

        StudentEntity studentEntity = new StudentEntity();

        studentEntity.setGroup( groupDtoToGroup( studentDto.getGroup() ) );
        studentEntity.setId( studentDto.getId() );
        studentEntity.setFirstname( studentDto.getFirstname() );
        studentEntity.setLastname( studentDto.getLastname() );
        studentEntity.setPatronymic( studentDto.getPatronymic() );
        studentEntity.setMarks( markDtoListToMarkEntityList( studentDto.getMarks() ) );

        return studentEntity;
    }

    @Override
    public StudentDto studentToDto(StudentEntity student) {
        if ( student == null ) {
            return null;
        }

        StudentDto studentDto = new StudentDto();

        studentDto.setGroup( groupToGroupDto( student.getGroup() ) );
        studentDto.setId( student.getId() );
        studentDto.setFirstname( student.getFirstname() );
        studentDto.setLastname( student.getLastname() );
        studentDto.setPatronymic( student.getPatronymic() );
        studentDto.setMarks( markEntityListToMarkDtoList( student.getMarks() ) );

        return studentDto;
    }

    @Override
    public List<StudentDto> studentToDto(List<StudentEntity> student) {
        if ( student == null ) {
            return null;
        }

        List<StudentDto> list = new ArrayList<StudentDto>( student.size() );
        for ( StudentEntity studentEntity : student ) {
            list.add( studentToDto( studentEntity ) );
        }

        return list;
    }

    @Override
    public GroupDto groupToDto(Group group) {
        if ( group == null ) {
            return null;
        }

        GroupDto groupDto = new GroupDto();

        groupDto.setGroupId( group.getId() );
        groupDto.setGroupName( group.getName() );

        return groupDto;
    }

    @Override
    public List<GroupDto> groupToDto(List<Group> groups) {
        if ( groups == null ) {
            return null;
        }

        List<GroupDto> list = new ArrayList<GroupDto>( groups.size() );
        for ( Group group : groups ) {
            list.add( groupToDto( group ) );
        }

        return list;
    }

    @Override
    public SubjectDto subjectToDto(SubjectEntity subject) {
        if ( subject == null ) {
            return null;
        }

        SubjectDto subjectDto = new SubjectDto();

        subjectDto.setSubjectId( subject.getId() );
        subjectDto.setSubjectName( subject.getName() );

        return subjectDto;
    }

    @Override
    public List<SubjectDto> subjectToDto(List<SubjectEntity> subjects) {
        if ( subjects == null ) {
            return null;
        }

        List<SubjectDto> list = new ArrayList<SubjectDto>( subjects.size() );
        for ( SubjectEntity subjectEntity : subjects ) {
            list.add( subjectToDto( subjectEntity ) );
        }

        return list;
    }

    protected Group groupDtoToGroup(GroupDto groupDto) {
        if ( groupDto == null ) {
            return null;
        }

        Group group = new Group();

        group.setId( groupDto.getGroupId() );
        group.setName( groupDto.getGroupName() );

        return group;
    }

    protected MarkEntity markDtoToMarkEntity(MarkDto markDto) {
        if ( markDto == null ) {
            return null;
        }

        MarkEntity markEntity = new MarkEntity();

        markEntity.setId( markDto.getId() );
        markEntity.setSubjectId( markDto.getSubjectId() );
        markEntity.setValue( markDto.getValue() );
        markEntity.setMarkTypeId( markDto.getMarkTypeId() );
        markEntity.setStudentId( markDto.getStudentId() );

        return markEntity;
    }

    protected List<MarkEntity> markDtoListToMarkEntityList(List<MarkDto> list) {
        if ( list == null ) {
            return null;
        }

        List<MarkEntity> list1 = new ArrayList<MarkEntity>( list.size() );
        for ( MarkDto markDto : list ) {
            list1.add( markDtoToMarkEntity( markDto ) );
        }

        return list1;
    }

    protected GroupDto groupToGroupDto(Group group) {
        if ( group == null ) {
            return null;
        }

        GroupDto groupDto = new GroupDto();

        groupDto.setGroupId( group.getId() );
        groupDto.setGroupName( group.getName() );

        return groupDto;
    }

    protected MarkDto markEntityToMarkDto(MarkEntity markEntity) {
        if ( markEntity == null ) {
            return null;
        }

        MarkDto markDto = new MarkDto();

        markDto.setId( markEntity.getId() );
        markDto.setSubjectId( markEntity.getSubjectId() );
        markDto.setMarkTypeId( markEntity.getMarkTypeId() );
        markDto.setValue( markEntity.getValue() );
        markDto.setStudentId( markEntity.getStudentId() );

        return markDto;
    }

    protected List<MarkDto> markEntityListToMarkDtoList(List<MarkEntity> list) {
        if ( list == null ) {
            return null;
        }

        List<MarkDto> list1 = new ArrayList<MarkDto>( list.size() );
        for ( MarkEntity markEntity : list ) {
            list1.add( markEntityToMarkDto( markEntity ) );
        }

        return list1;
    }
}
