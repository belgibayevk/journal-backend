package com.task.journal.mapper;

import com.task.journal.dto.GroupDto;
import com.task.journal.dto.StudentDto;
import com.task.journal.dto.SubjectDto;
import com.task.journal.entity.Group;
import com.task.journal.entity.StudentEntity;
import com.task.journal.entity.SubjectEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Component
public interface JournalMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "firstname", target = "firstname")
    @Mapping(source = "lastname", target = "lastname")
    @Mapping(source = "patronymic", target = "patronymic")
    @Mapping(source = "group.groupId", target = "group.id")
    @Mapping(source = "group.groupName", target = "group.name")

    StudentEntity studentDtoToStudentEntity(StudentDto studentDto);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "firstname", target = "firstname")
    @Mapping(source = "lastname", target = "lastname")
    @Mapping(source = "patronymic", target = "patronymic")
    @Mapping(source = "group.id", target = "group.groupId")
    @Mapping(source = "group.name", target = "group.groupName")
    @Mapping(source = "marks", target = "marks")
    StudentDto studentToDto(StudentEntity student);

    List<StudentDto> studentToDto(List<StudentEntity> student);

    @Mapping(source = "id", target = "groupId")
    @Mapping(source = "name", target = "groupName")
    GroupDto groupToDto(Group group);

    List<GroupDto> groupToDto(List<Group> groups);

    @Mapping(source = "id", target = "subjectId")
    @Mapping(source = "name", target = "subjectName")
    SubjectDto subjectToDto(SubjectEntity subject);

    List<SubjectDto> subjectToDto(List<SubjectEntity> subjects);
}
