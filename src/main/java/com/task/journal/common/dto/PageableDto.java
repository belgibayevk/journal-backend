package com.task.journal.common.dto;

import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

public class PageableDto<T> implements Serializable {

    private List<T> dtoList;
    private Long totalElements;

    public PageableDto() {
    }

    public PageableDto(Page<T> dto) {
        this.setTotalElements(dto.getTotalElements());
        this.dtoList = dto.getContent();
    }

    public PageableDto(List<T> dtoList, Long totalElements) {
        this.dtoList = dtoList;
        this.totalElements = totalElements;
    }

    public List<T> getDtoList() {
        return dtoList;
    }

    public void setDtoList(List<T> dtoList) {
        this.dtoList = dtoList;
    }

    public PageableDto<T> setList(List<T> dtoList) {
        this.setDtoList(dtoList);
        return this;
    }

    public Long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

}
