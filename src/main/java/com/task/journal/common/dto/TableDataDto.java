package com.task.journal.common.dto;
import java.util.HashMap;
import java.util.Map;

public class TableDataDto {
    private int pageNumber;
    private int size;
    private Map<String, Object> filter;
    private Map<String, String> sorting;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSize() {
        return size;
    }

    public TableDataDto setSize(int size) {
        this.size = size;
        return this;
    }

    public Map<String, Object> getFilter() {
        return filter;
    }

    public void setFilter(Map<String, Object> filter) {
        this.filter = filter;
    }

    public Map<String, String> getSorting() {
        return sorting;
    }

    public void setSorting(Map<String, String> sorting) {
        this.sorting = sorting;
    }

    public TableDataDto addFilterParam(String key, Object value) {
        if (filter == null) {
            filter = new HashMap<>();
        }
        filter.put(key, value);
        return this;
    }

    @Override
    public String toString() {
        return "TableDataDto{" +
                "pageNumber=" + pageNumber +
                ", size=" + size +
                ", filter=" + filter +
                ", sorting=" + sorting +
                '}';
    }
}
