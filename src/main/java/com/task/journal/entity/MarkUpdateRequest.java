package com.task.journal.entity;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
public class MarkUpdateRequest {
    private Long studentId;
    private Long subjectId;
    private Map<Integer, Integer> editedMarks;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Map<Integer, Integer> getEditedMarks() {
        return editedMarks;
    }

    public void setEditedMarks(Map<Integer, Integer> editedMarks) {
        this.editedMarks = editedMarks;
    }
}