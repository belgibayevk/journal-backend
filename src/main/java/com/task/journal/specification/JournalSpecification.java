package com.task.journal.specification;

import com.task.journal.entity.StudentEntity;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JournalSpecification {
    public static Specification<StudentEntity> findByCriteria(final Map<String, Object> searchMap) {
        return (root, criteriaQuery, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (searchMap != null) {
                String search = (String) searchMap.get("search");
                if (search != null && !search.trim().isEmpty()) {
                    predicates.add(cb.like(root.get("nameRu"), "%" + search.trim() + "%"));
                }
            }
            return cb.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
