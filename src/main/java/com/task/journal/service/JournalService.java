package com.task.journal.service;

import com.task.journal.dto.GroupDto;
import com.task.journal.dto.StudentDto;
import com.task.journal.dto.SubjectDto;
import com.task.journal.entity.Group;
import com.task.journal.entity.MarkUpdateRequest;
import com.task.journal.entity.StudentEntity;
import com.task.journal.entity.SubjectEntity;
import com.task.journal.mapper.JournalMapper;
import com.task.journal.repository.GroupRepository;
import com.task.journal.repository.MarkRepository;
import com.task.journal.repository.StudentRepository;
import com.task.journal.repository.SubjectRepository;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class JournalService {

    private final StudentRepository studentRepository;
    private final SubjectRepository subjectRepository;
    private final GroupRepository groupRepository;
    private final MarkRepository markRepository;
    private final JournalMapper journalMapper;

    public JournalService(StudentRepository studentRepository, SubjectRepository subjectRepository, GroupRepository groupRepository, MarkRepository markRepository, JournalMapper journalMapper) {
        this.studentRepository = studentRepository;
        this.subjectRepository = subjectRepository;
        this.groupRepository = groupRepository;
        this.markRepository = markRepository;
        this.journalMapper = journalMapper;
    }

    public void addStudent(StudentDto studentDto) {
        StudentEntity student = journalMapper.studentDtoToStudentEntity(studentDto);
        try {
            this.studentRepository.insertStudent(student);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<StudentDto> getMarksForStudentAndSubject(Long groupId, Long subjectId) {
        List<StudentEntity> students = markRepository.getMarksForStudentAndSubject(groupId, subjectId);
        return journalMapper.studentToDto(students);
    }

    public List<StudentDto> getStudents() {
        List<StudentEntity> students = studentRepository.getStudents();
        return journalMapper.studentToDto(students);
    }

    public List<StudentEntity> getStudent(int pageNumber, int pageSize) throws SQLException {
        return studentRepository.findStudents(pageNumber, pageSize);
    }


    public long getTotalStudentCount() throws SQLException {
        return studentRepository.getTotalStudentCount();
    }


    public List<GroupDto> getGroups() {
        List<Group> groups = groupRepository.getGroups();
        return journalMapper.groupToDto(groups);
    }

    public List<SubjectDto> getSubjects() {
        List<SubjectEntity> subjects = subjectRepository.getSubjects();
        return journalMapper.subjectToDto(subjects);
    }

    public List<StudentDto> getStudentsByGroupId(Long id) {
        List<StudentEntity> students = studentRepository.getStudentsByGroupId(id);
        return journalMapper.studentToDto(students);
    }

    public List<StudentDto> getStudentsBySubjectId(Long id) {
        List<StudentEntity> students = studentRepository.getStudentsBySubjectId(id);
        return journalMapper.studentToDto(students);
    }

    public void updateMarks(MarkUpdateRequest request) {
        try {
            this.markRepository.updateMarks(request);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteMark(MarkUpdateRequest request) {
        try {
            this.markRepository.deleteMarks(request);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<StudentEntity> searchStudents(String search) {
        return studentRepository.searchStudents(search);
    }
}
