package com.task.journal.enums;

public enum MarkType {
    AVERAGE_MARK(1, "Средная текущая"),
    RATING(2, "Рейтинг"),
    EXAM_MARK(3, "Экзамен"),
    TOTAL_MARK(4, "Итоговая");

    private final int id;
    private final String description;

    MarkType(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public static MarkType fromValue(int mark_type) {
        for (MarkType type : MarkType.values()) {
            if (type.getId() == mark_type) {
                return type;
            }
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}