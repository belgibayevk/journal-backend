package com.task.journal.controller;

import com.task.journal.dto.GroupDto;
import com.task.journal.dto.StudentDto;
import com.task.journal.dto.SubjectDto;
import com.task.journal.entity.MarkUpdateRequest;
import com.task.journal.entity.StudentEntity;
import com.task.journal.service.JournalService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/journal")
public class JournalController {

    private final JournalService journalService;

    public JournalController(JournalService journalService) {
        this.journalService = journalService;
    }


    @GetMapping("/list")
    public ResponseEntity<List<StudentEntity>> getStudents(
            @RequestParam int pageNumber,
            @RequestParam(defaultValue = "10") int pageSize) {
        try {
            List<StudentEntity> students = journalService.getStudent(pageNumber, pageSize);
            long totalElements = journalService.getTotalStudentCount();
            return ResponseEntity.ok(students);
        } catch (SQLException e) {
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/totalCount")
    public ResponseEntity<Long> getTotalStudentCount() {
        try {
            long totalStudentCount = journalService.getTotalStudentCount();
            return ResponseEntity.ok(totalStudentCount);
        } catch (SQLException e) {
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<StudentDto>> getAllStudent() {
        List<StudentDto> students = journalService.getStudents();
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<List<StudentEntity>> searchStudents(@RequestParam String search) {
        List<StudentEntity> students = journalService.searchStudents(search);
        return ResponseEntity.ok(students);
    }

    @PostMapping()
    public ResponseEntity<StudentDto> addStudent(@RequestBody StudentDto studentDto) {
        journalService.addStudent(studentDto);
        try {
            return new ResponseEntity<>(studentDto, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    // Journal page

    @GetMapping("/groups")
    public ResponseEntity<List<GroupDto>> getGroups() {
        List<GroupDto> groups = journalService.getGroups();
        return new ResponseEntity<>(groups, HttpStatus.OK);
    }

    @GetMapping("/subjects")
    public ResponseEntity<List<SubjectDto>> getSubjects() {
        List<SubjectDto> subjects = journalService.getSubjects();
        return new ResponseEntity<>(subjects, HttpStatus.OK);
    }

    @GetMapping("/students/{groupId}")
    public ResponseEntity<List<StudentDto>> getStudentByGroupId(@PathVariable("groupId") Long groupId) {
        List<StudentDto> studentDto = journalService.getStudentsByGroupId(groupId);
        return new ResponseEntity<>(studentDto, HttpStatus.OK);
    }

    @GetMapping("/studentsBySubject/{groupId}")
    public ResponseEntity<List<StudentDto>> getStudentBySubjectId(@PathVariable("subjectId") Long subjectId) {
        List<StudentDto> studentDto = journalService.getStudentsBySubjectId(subjectId);
        return new ResponseEntity<>(studentDto, HttpStatus.OK);
    }

    @GetMapping("/marks/{groupId}/{subjectId}")
    public ResponseEntity<List<StudentDto>> getMarksForStudentAndSubject(@PathVariable Long groupId, @PathVariable Long subjectId) {
        List<StudentDto> mark = journalService.getMarksForStudentAndSubject(groupId, subjectId);
        return new ResponseEntity<>(mark, HttpStatus.OK);
    }


    @PutMapping()
    public ResponseEntity<String> updateMark(@RequestBody MarkUpdateRequest request) {
        try {
            journalService.updateMarks(request);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ошибка при обновлении оценок: " + e.getMessage());
        }
    }

    @PostMapping("/createMark")
    public ResponseEntity<String> createMark(@RequestBody MarkUpdateRequest request) {
        try {
            journalService.updateMarks(request);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при создании оценки: " + e.getMessage());
        }
    }

    @PutMapping("/deleteMark")
    public ResponseEntity<String> deleteMark(@RequestBody MarkUpdateRequest request) {
        try {
            journalService.deleteMark(request);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при удалении оценки: " + e.getMessage());
        }
    }
}
