package com.task.journal.utils;

import com.task.journal.common.dto.TableDataDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PageableUtils {
    public static Pageable getPageRequest(TableDataDto dataDto) {
        return getPageRequestOrderById(dataDto);
    }

    public static Pageable getPageRequestOrderById(TableDataDto dataDto) {
        if (dataDto.getSorting() != null && !dataDto.getSorting().isEmpty()) {
            List<Sort.Order> orders = new ArrayList<>();
            for (String key : dataDto.getSorting().keySet()) {
                orders.add(new Sort.Order(Sort.Direction.fromString(dataDto.getSorting().get(key)), key));
            }
            Sort sort = Sort.by(orders);
            return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), sort);
        }
        return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), Sort.Direction.DESC, "id");
    }

    public static Pageable getPageRequestWithOrder(TableDataDto dataDto, Sort.Direction order, String... fields) {
        if (Objects.isNull(order)) {
            return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), Sort.Direction.DESC, fields);
        }
        return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), order, fields);
    }

    public static Pageable getPageRequestWithOrder(TableDataDto dataDto, Sort sort) {
        if (Objects.isNull(sort)) {
            return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize());
        }
        return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), sort);
    }

    public static Pageable getPageableSortedAscById(TableDataDto dataDto) {
        return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize(), Sort.by(Sort.Direction.ASC, "id"));
    }

    public static Pageable getPageableOnlySizes(TableDataDto dataDto) {
        return PageRequest.of(dataDto.getPageNumber(), dataDto.getSize());
    }
}
