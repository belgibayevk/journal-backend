package com.task.journal.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude()
public class GroupDto {
    private Long groupId;
    private String groupName;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static GroupDto fromGroupId(Long groupId) {
        GroupDto group = new GroupDto();
        group.setGroupId(groupId);
        return group;
    }
}
