package com.task.journal.repository;

import com.task.journal.entity.Group;
import com.task.journal.entity.StudentEntity;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    private Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/journal";
        String username = "root";
        String password = "root";
        return DriverManager.getConnection(url, username, password);
    }

    public boolean insertStudent(StudentEntity student) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(
                     "INSERT INTO students(lastname, firstname, patronymic, group_id) VALUES (?,?,?,?)")) {
            ps.setString(1, student.getLastname());
            ps.setString(2, student.getFirstname());
            ps.setString(3, student.getPatronymic());
            ps.setLong(4, student.getGroup().getId());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void save(StudentEntity student) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement(
                     "UPDATE students SET lastname = ?, firstname = ?, patronymic = ?, group_id = ? WHERE id = ?")) {
            ps.setString(1, student.getLastname());
            ps.setString(2, student.getFirstname());
            ps.setString(3, student.getPatronymic());
            ps.setLong(4, student.getGroup().getId());
            ps.setLong(5, student.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<StudentEntity> getStudents() {
        List<StudentEntity> students = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement st = connection.prepareStatement("SELECT s.*, g.name AS group_name FROM students s " +
                     " join `groups` g on s.group_id = g.id ")) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                StudentEntity student = new StudentEntity();
                student.setId(rs.getLong("id"));
                student.setLastname(rs.getString("lastname"));
                student.setFirstname(rs.getString("firstname"));
                student.setPatronymic(rs.getString("patronymic"));
                Group group = new Group();
                group.setName(rs.getString("group_name"));
                student.setGroup(group);
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    public List<StudentEntity> findStudents(int pageNumber, int pageSize) throws SQLException {
        List<StudentEntity> students = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT s.id, s.firstname, s.lastname, s.patronymic, g.name AS group_name " +
                     " FROM students s " +
                     " JOIN `groups` g ON s.group_id = g.id " +
                     " LIMIT ? OFFSET ?")) {
            statement.setInt(1, pageSize);
            statement.setInt(2, (pageNumber - 1) * pageSize);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                StudentEntity student = new StudentEntity();
                student.setId(rs.getLong("id"));
                student.setFirstname(rs.getString("firstname"));
                student.setLastname(rs.getString("lastname"));
                student.setPatronymic(rs.getString("patronymic"));
                Group group = new Group();
                group.setName(rs.getString("group_name"));
                student.setGroup(group);
                students.add(student);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }
    
    public int getTotalStudentCount() throws SQLException {
        String countQuery = "SELECT COUNT(*) FROM students";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(countQuery)) {
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    public StudentEntity getStudentsByStudentId(Long id) {
        StudentEntity student = new StudentEntity();
        try (Connection connection = getConnection();
            PreparedStatement st = connection.prepareStatement("SELECT * FROM students WHERE id = ?")){
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return mapResultSetToStudent(rs);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return student;
    }

    public List<StudentEntity> getStudentsByGroupId(Long id) {
        List<StudentEntity> students = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement("SELECT * FROM students WHERE group_id = ?")) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                students.add(mapResultSetToStudent(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    public List<StudentEntity> getStudentsBySubjectId(Long id) {
        List<StudentEntity> students = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement ps = connection.prepareStatement("select s.*, sb.* " +
                     "from subjects sb " +
                     "         join studygroups st on sb.id = st.subject_id " +
                     "         join `groups` g on g.id = st.group_id " +
                     "         left join students s on g.id = s.group_id " +
                     "where s.group_id = ?")) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                students.add(mapResultSetToStudent(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    private StudentEntity mapResultSetToStudent(ResultSet resultSet) throws SQLException {
        StudentEntity student = new StudentEntity();
        student.setId(resultSet.getLong("id"));
        student.setLastname(resultSet.getString("lastname"));
        student.setFirstname(resultSet.getString("firstname"));
        student.setPatronymic(resultSet.getString("patronymic"));
        return student;
    }

    public List<StudentEntity> searchStudents(String search) {
        List<StudentEntity> students = new ArrayList<>();
        String sql = "SELECT s.id, s.firstname, s.lastname, s.patronymic, g.name AS group_name " +
                "FROM students s " +
                "JOIN `groups` g ON s.group_id = g.id ";
        if (search != null && !search.isEmpty()) {
            sql += "WHERE s.firstname LIKE ? OR s.lastname LIKE ?";
        }
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            if (search != null && !search.isEmpty()) {
                String likeQuery = "%" + search + "%";
                statement.setString(1, likeQuery);
                statement.setString(2, likeQuery);
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                StudentEntity student = new StudentEntity();
                student.setId(rs.getLong("id"));
                student.setFirstname(rs.getString("firstname"));
                student.setLastname(rs.getString("lastname"));
                student.setPatronymic(rs.getString("patronymic"));
                Group group = new Group();
                group.setName(rs.getString("group_name"));
                student.setGroup(group);
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }
}
