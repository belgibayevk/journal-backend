package com.task.journal.repository;

import com.task.journal.entity.MarkEntity;
import com.task.journal.entity.MarkUpdateRequest;
import com.task.journal.entity.StudentEntity;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MarkRepository {

    private final DataSource dataSource;
    private final StudentRepository studentRepository;

    public MarkRepository(DataSource dataSource, StudentRepository studentRepository) {
        this.dataSource = dataSource;
        this.studentRepository = studentRepository;
    }

    public List<MarkEntity> getMarksByStudentIdAndSubjectId(Long studentId, Long subjectId) {
        List<MarkEntity> marks = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            String sql = "select s2.id, s2.firstname, s2.lastname, s2.patronymic, m.* from studygroups  s " +
                    "left join students s2 on s.group_id = s2.group_id " +
                    "left join marks m on s2.id = m.student_id and m.subject_id = s.subject_id " +
                    "where m.student_id = ? and m.subject_id = ? ";
            try (PreparedStatement st = connection.prepareStatement(sql)) {
                st.setLong(1, studentId);
                st.setLong(2, subjectId);
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        MarkEntity mark = new MarkEntity();
                        mark.setId(rs.getLong("id"));
                        mark.setSubjectId(rs.getLong("subject_id"));
                        mark.setMarkTypeId(rs.getInt("mark_type"));
                        mark.setValue(rs.getInt("value"));
                        marks.add(mark);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return marks;
    }

    public List<StudentEntity> getMarksForStudentAndSubject(Long groupId, Long subjectId) {
        Map<Long, StudentEntity> studentEntityMap = new HashMap<>();
        String addParam = "and  s.subject_id = ?";
        try (Connection connection = dataSource.getConnection()) {
            String sql = "select s2.id, s2.firstname, s2.lastname, s2.patronymic, m.* from studygroups  s " +
                    " join students s2 on s.group_id = s2.group_id " +
                    " join marks m on s2.id = m.student_id and m.subject_id = s.subject_id " +
                    "where s2.group_id = ? ";

            StringBuilder builder = new StringBuilder();
            builder.append(sql);
            if (subjectId > 0) builder.append(addParam);
            try (PreparedStatement st = connection.prepareStatement(builder.toString())) {
                st.setLong(1, groupId);
                if (subjectId > 0) st.setLong(2, subjectId);
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        MarkEntity mark = new MarkEntity();
                        mark.setId(rs.getLong("id"));
                        mark.setSubjectId(rs.getLong("subject_id"));
                        mark.setMarkTypeId(rs.getInt("mark_type"));
                        mark.setValue(rs.getInt("value"));

                        if (!studentEntityMap.containsKey(rs.getLong("id"))) {
                            StudentEntity student = new StudentEntity();
                            student.setId(rs.getLong("id"));
                            student.setLastname(rs.getString("lastname"));
                            student.setFirstname(rs.getString("firstname"));
                            student.setPatronymic(rs.getString("patronymic"));
                            student.setMarks(new ArrayList<>());
                            studentEntityMap.put(rs.getLong("id"), student);
                        }
                        StudentEntity student = studentEntityMap.get(rs.getLong("id"));
                        student.getMarks().add(mark);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentEntityMap.values().stream().toList();
    }

    public void updateMarks(MarkUpdateRequest request) throws SQLException {
        List<MarkEntity> marks = getMarksByStudentIdAndSubjectId(request.getStudentId(), request.getSubjectId());
        Map<Integer, Integer> editedMarks = request.getEditedMarks();
        if (editedMarks.keySet().size() != marks.size()) {
            createMarks(request);
        } else {
            for (MarkEntity mark : marks) {
                Integer markTypeId = mark.getMarkTypeId();
                updateMark(request.getStudentId(), request.getSubjectId(), markTypeId, editedMarks.get(markTypeId));
            }
        }
    }

    public void deleteMarks(MarkUpdateRequest request) throws SQLException {
        StudentEntity student = studentRepository.getStudentsByStudentId(request.getStudentId());
        List<MarkEntity> marks = getMarksByStudentIdAndSubjectId(student.getId(), request.getSubjectId());
        Map<Integer, Integer> editedMarks = request.getEditedMarks();

        for (MarkEntity mark : marks) {
            Integer markTypeId = mark.getMarkTypeId();
            Integer editedValue = editedMarks.get(markTypeId);

            if (editedValue == null) { // Если значение приходит как null из фронта
                // Проверяем, существует ли оценка в базе данных
                boolean markExists = getCountMarks(request.getStudentId(), request.getSubjectId(), markTypeId);

                if (markExists) {
                    // Выполняем удаление
                    deleteMark(request.getStudentId(), request.getSubjectId(), markTypeId);
                }
            }
        }
    }


    public void createMarks(MarkUpdateRequest request) throws SQLException {
        Map<Integer, Integer> editedMarks = request.getEditedMarks();
        for (Integer key : editedMarks.keySet()) {
            boolean markExist = getCountMarks(request.getStudentId(), request.getSubjectId(), key);
            if (markExist) {
                updateMark(request.getStudentId(), request.getSubjectId(), key, editedMarks.get(key));
            } else {
                createMark(request.getStudentId(), request.getSubjectId(), key, editedMarks.get(key));
            }
        }
    }

    private boolean createMark(Long studentId, Long subjectId, Integer markTypeId, Integer value) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            String sql = "INSERT INTO marks (student_id, subject_id, mark_type,value) VALUES (?, ?, ?, ?)";
            try (PreparedStatement st = connection.prepareStatement(sql)) {
                st.setLong(1, studentId);
                st.setLong(2, subjectId);
                st.setInt(3, markTypeId);
                st.setInt(4, value);
                int rowsAffected = st.executeUpdate();
                return rowsAffected > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean getCountMarks(Long studentId, Long subjectId, int markTypeId) {
        try (Connection connection = dataSource.getConnection()) {
            String sql = "SELECT COUNT(*) FROM marks WHERE student_id = ? AND subject_id = ? AND mark_type = ?;";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setLong(1, studentId);
                statement.setLong(2, subjectId);
                statement.setInt(3, markTypeId);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateMark(Long studentId, Long subjectId, int markTypeId, int value) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            String sql = "UPDATE marks SET value = ? WHERE student_id = ? AND subject_id = ? AND mark_type = ?";
            try (PreparedStatement st = connection.prepareStatement(sql)) {
                st.setInt(1, value);
                st.setLong(2, studentId);
                st.setLong(3, subjectId);
                st.setInt(4, markTypeId);
                int rowsUpdated = st.executeUpdate();
                return rowsUpdated > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteMark(Long studentId, Long subjectId, int markTypeId) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            String sql = "DELETE FROM marks  WHERE student_id = ? AND subject_id = ? AND mark_type = ?";
            try (PreparedStatement st = connection.prepareStatement(sql)) {
                st.setLong(1, studentId);
                st.setLong(2, subjectId);
                st.setInt(3, markTypeId);
                int rowsUpdated = st.executeUpdate();
                return rowsUpdated > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}