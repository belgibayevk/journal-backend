package com.task.journal.repository;

import com.task.journal.entity.SubjectEntity;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SubjectRepository {

    private final DataSource dataSource;

    public SubjectRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<SubjectEntity> getSubjects() {
        List<SubjectEntity> subjects = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement st = connection.prepareStatement("SELECT * FROM subjects")) {
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectEntity subject = new SubjectEntity();
                subject.setId(rs.getLong("id"));
                subject.setName(rs.getString("name"));
                subjects.add(subject);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjects;
    }
}
